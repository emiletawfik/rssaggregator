package rssaggregator;
 
import static spark.Spark.*;

import com.google.gson.Gson;
import java.util.Date;
import java.util.Timer;

import rssaggregator.models.FeedAggregation;
import rssaggregator.models.Token;
import rssaggregator.models.User;
import rssaggregator.services.FeedAggregatorService;
import rssaggregator.services.FeedCronManagement;
import rssaggregator.services.ResponseStatus;
import rssaggregator.services.StandardResponse;
import rssaggregator.services.UserService;
import spark.Request;
import spark.Spark;
import rssaggregator.services.M;


public class Main {

    public static UserService userService = new UserService();
    public static FeedAggregatorService faService = new FeedAggregatorService();
    public static Timer timer = new Timer();

    public static Token getCurrentUser(Request req) throws Exception {
        String t = req.headers("Authorization");
        if (t == null || t.isEmpty()) {
            throw new Exception("You must specify authorization header to use this call");
        }
        Token token = M.datastore.get(Token.class, t);
        if (token == null) {
            throw new Exception("Your are authentified with a wrong token");
        }
        if (token.getExpirationDate() == null || token.getExpirationDate().before(new Date())) {
            throw new Exception("This token is expired please use a new one");
        }
        return token;
    }

    public static void main(String[] args) {
        Gson gson = new Gson();
        M.MongoBegin();
        Spark.exception(Exception.class, (exception, request, response) -> {
            exception.printStackTrace();
        });
        enableCORS("*", "GET,PUT,DELETE,POST,OPTIONS", "Access-Control-Allow-Origin, "
                + "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        FeedCronManagement.getInstance();

        post("/users", (req, res) -> {
            res.type("application/json");
            try {
                User user = gson.fromJson(req.body(), User.class);
                user.setPassword(user.getPassword());
				return new StandardResponse(ResponseStatus.SUCCESS, gson.toJsonTree(userService.addUser(user)));
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			}
        }, gson::toJson);

        post("/users/changepassword", (req, res) -> {
            res.type("application/json");
            try {
                User user = gson.fromJson(req.body(), User.class);
                Token tok = getCurrentUser(req);
				return new StandardResponse(ResponseStatus.SUCCESS, gson.toJsonTree(userService.changePassword(tok, user.getPassword())));
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			}
        }, gson::toJson);

        delete("/users/delete", (req, res) -> {
            res.type("application/json");
            try {
                Token tok = getCurrentUser(req);
                userService.deleteUser(tok);
				return new StandardResponse(ResponseStatus.SUCCESS);
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			} 
        }, gson::toJson);

        post("/newToken", (req, res) -> {
            res.type("application/json");
            try {
                User user = gson.fromJson(req.body(), User.class);
                user.setPassword(user.getPassword());
				return new StandardResponse(ResponseStatus.SUCCESS, gson.toJsonTree(userService.newToken(user)));
			} catch (Exception e) {
                res.status(400);
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			}
        }, gson::toJson);

        post("/feedaggregators", (req, res) -> {
            res.type("application/json");
            try {
                FeedAggregation fa = gson.fromJson(req.body(), FeedAggregation.class);
                Token tok = getCurrentUser(req);
				return new StandardResponse(ResponseStatus.SUCCESS, gson.toJsonTree(faService.createFeedAggregation(tok, fa)));
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			} 
        }, gson::toJson);

        delete("/feedaggregators/:uuid", (req, res) -> {
            res.type("application/json");
            try {
                String uuid = req.params(":uuid");
                Token tok = getCurrentUser(req);
                faService.deleteFeedAggregation(tok, uuid);
				return new StandardResponse(ResponseStatus.SUCCESS);
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			} 
        }, gson::toJson);

        get("/feedaggregators", (req, res) -> {
            res.type("application/json");
            try {
                Token tok = getCurrentUser(req);
				return new StandardResponse(ResponseStatus.SUCCESS, gson.toJsonTree(faService.getFeedAggregations(tok)));
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			} 
        }, gson::toJson);

        get("/feedaggregators/:uuid", (req, res) -> {
            res.type("application/json");
            try {
                String uuid = req.params(":uuid");
                Token tok = getCurrentUser(req);
				return new StandardResponse(ResponseStatus.SUCCESS, gson.toJsonTree(faService.getFeedMessages(tok, uuid)));
			} catch (Exception e) {
                res.status(400);
                e.printStackTrace();
                return new StandardResponse(ResponseStatus.ERROR, e.getMessage());
			} 
        }, gson::toJson);
    }

    /**
     * Enables CORS headers and preflight request responses
     * See @see <a href="https://www.html5rocks.com/en/tutorials/cors/">https://www.html5rocks.com/en/tutorials/cors/</a>  for more informations
     * @param origin Origin domain (can be * to enable CORS for every domain)
     * @param methods List of methods to be authorized (GET, PUT, POST, DELETE, etc)
     * @param headers List of custom headers to be authorized (Content-Type, Authorization, etc)
     */
    private static void enableCORS(final String origin, final String methods, final String headers) {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
        });
    }
}