package rssaggregator.services;

import java.util.Calendar;
import java.util.Date;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;

import rssaggregator.models.FeedAggregation;
import rssaggregator.models.Token;
import rssaggregator.models.User;

public class UserService {

    public User addUser(User user) throws Exception {
        Query<User> query = M.datastore.find(User.class, "username", user.getUsername());
        if (query.countAll() > 0) {
            throw new Exception("Username already used");
        }
        Key<User> key = M.datastore.save(user);
        return M.datastore.getByKey(User.class, key);
    }

    public Token newToken(User user) throws Exception {
        DBCollection userCollections = M.datastore.getCollection(User.class);

        BasicDBObject userDBObj = new BasicDBObject();
        userDBObj.put("username", user.getUsername());
        userDBObj.put("password", user.getPassword());
        DBCursor cursor = userCollections.find(userDBObj);
        if (cursor.count() == 0) {
            throw new Exception("Can't find username or password");
        }
        if (cursor.hasNext() == false) {
            throw new Exception("Can't retrieve user uuid");
        }
        DBObject retrievedUser = cursor.next();
        String uuid = retrievedUser.get("_id").toString();
        Token tok = new Token();
        tok.setUserUuid(uuid);
        Date currDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currDate);
        c.add(Calendar.DATE, 7);
        tok.setExpirationDate(c.getTime());
        Key<Token> key = M.datastore.save(tok);
        return M.datastore.getByKey(Token.class, key);
    }

    public User changePassword(Token token, String password) throws Exception {
        User user = M.datastore.get(User.class, token.getUserUuid());
        if (user == null) {
            throw new Exception("The user associated to the token didn't exist");
        }
        user.setPassword(password);
        Key<User> key = M.datastore.save(user);
        M.datastore.delete(M.datastore.find(Token.class, "user_uuid", token.getUserUuid()));
        return M.datastore.getByKey(User.class, key);
    }

    public void deleteUser(Token token) throws Exception {
        User user = M.datastore.get(User.class, token.getUserUuid());
        if (user == null) {
            throw new Exception("The user associated to the token didn't exist");
        }
        M.datastore.delete(M.datastore.find(FeedAggregation.class, "user_uuid", user.getUuid()));
        M.datastore.delete(M.datastore.find(Token.class, "user_uuid", token.getUserUuid()));
        M.datastore.delete(user);
    }
}
