package rssaggregator.services;

import com.mongodb.MongoClient;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import rssaggregator.models.User;
import rssaggregator.models.Feed;
import rssaggregator.models.FeedAggregation;
import rssaggregator.models.FeedMessage;
import rssaggregator.models.Token;

public class M {
    public static MongoClient client;
    public static Morphia morphia;
    public static Datastore datastore;

    public static void MongoBegin() {
        M.client = new MongoClient("mongo", 27017);
        M.morphia = new Morphia();
        M.morphia.map(User.class);
        M.morphia.map(Token.class);
        M.morphia.map(FeedMessage.class);
        M.morphia.map(Feed.class);
        M.morphia.map(FeedAggregation.class);
        M.datastore = morphia.createDatastore(client, "rssaggregator");
        M.datastore.ensureIndexes();
    }
}