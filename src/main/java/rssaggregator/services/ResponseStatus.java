package rssaggregator.services;

public enum ResponseStatus {
    SUCCESS ("Success"),
    ERROR ("Error");
  
    private String status;
    
    ResponseStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return this.status;
    }
}