package rssaggregator.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;
import rssaggregator.models.Feed;
import rssaggregator.models.FeedAggregation;
import rssaggregator.models.Token;
import rssaggregator.models.FeedMessage;

public class FeedAggregatorService {
    public FeedAggregation createFeedAggregation(Token tok, FeedAggregation fa) throws Exception {
        if (fa.getUserUuid() == null || fa.getUserUuid().isEmpty()) {
            fa.setUserUuid(tok.getUserUuid());
        }
        Key<FeedAggregation> key = (Key<FeedAggregation>) M.datastore.exists(fa);
        if (key != null) {
            FeedAggregation faCheck = M.datastore.getByKey(FeedAggregation.class, key);
            if (!tok.getUserUuid().equals(faCheck.getUserUuid())) {
                throw new Exception("You can't update this aggregation because your are not his owner");
            }
        }
        if (fa.getName() == null || fa.getName().length() < 4) {
            throw new Exception("You must specify a name and his length must be at least 4");
        }
        key = M.datastore.save(fa);
        fa = M.datastore.getByKey(FeedAggregation.class, key);
        updateFeedAggregations(fa);
        return fa;
    }

    public void deleteFeedAggregation(Token tok, String fa_uuid) throws Exception {
        Key<FeedAggregation> key = (Key<FeedAggregation>) M.datastore.exists(new FeedAggregation(fa_uuid));
        if (key != null) {
            FeedAggregation fa = M.datastore.getByKey(FeedAggregation.class, key);
            if (!tok.getUserUuid().equals(fa.getUserUuid())) {
                throw new Exception("You can't delete this aggregation because your are not his owner");
            }
            M.datastore.delete(FeedAggregation.class, fa.getUuid());
        }
    }

    public List<FeedAggregation> getFeedAggregations(Token tok) {
        Query<FeedAggregation> fas = M.datastore.find(FeedAggregation.class, "user_uuid", tok.getUserUuid()).order("-lastUpdate");
        return fas.asList();
    }

    public void updateFeedAggregations(FeedAggregation fa) throws ParseException {
        for (String feed_url : fa.getRssUrls()) {
            Feed feed = M.datastore.get(Feed.class, feed_url);
            if (feed == null || feed.getlastFetchDate() == null) {
                FeedCronManagement.getInstance().AddJob(feed_url);
            }
        }
    }

    public List<FeedMessage> getFeedMessages(Token tok, String fa_uuid) throws Exception {
        Key<FeedAggregation> key = (Key<FeedAggregation>) M.datastore.exists(new FeedAggregation(fa_uuid));
        if (key != null) {
            FeedAggregation fa = M.datastore.getByKey(FeedAggregation.class, key);
            if (!tok.getUserUuid().equals(fa.getUserUuid())) {
                throw new Exception("You can't access this aggregation because your are not his owner");
            }
            Query<FeedMessage> fms = M.datastore.find(FeedMessage.class).filter("rssurlsource in", fa.getRssUrls()).order("-pubDate");
            return fms.asList();
        } else {
            throw new Exception("FeedAggregation doesn't exist");
        }
    }
}