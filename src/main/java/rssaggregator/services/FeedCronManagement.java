package rssaggregator.services;

import com.mongodb.DBCollection;
import java.text.ParseException;
import java.time.Period;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.mongodb.morphia.query.Query;
import rssaggregator.models.Feed;
import rssaggregator.models.FeedAggregation;
import rssaggregator.models.utils.FeedParser;

public class FeedCronManagement {
    private Timer timer = new Timer();
    private static final long period = 5 * 60 * 1000;

    private FeedCronManagement() {
        for (Feed f :M.datastore.find(Feed.class).asList()) {
            timer.scheduleAtFixedRate(new FeedUpdateTask(f.getRssUrl()), 0, period);
        }
    }

    private static FeedCronManagement INSTANCE = null;
     
    public static synchronized FeedCronManagement getInstance()
    {           
        if (INSTANCE == null)
        {
            INSTANCE = new FeedCronManagement(); 
        }
        return INSTANCE;
    }

    public void AddJob(String url) {
        FeedUpdateTask task = new FeedUpdateTask(url);
        FeedUpdateTask taskCopy = new FeedUpdateTask(url);
        Timer tmp = new Timer();
        tmp.schedule(taskCopy, 0);
        timer.scheduleAtFixedRate(task, 0, period);
    }

    public class FeedUpdateTask extends TimerTask {
        String url;
    
        public FeedUpdateTask(String url) {
            this.url = url;
        }
    
        public void run() {
            try {
                Date lastBuildDate = null;
                Feed oldfeed = M.datastore.get(Feed.class, url);
                if (oldfeed != null) {
                    lastBuildDate = oldfeed.getLastBuildDate();
                }
                FeedParser parser = new FeedParser(url);
                Feed feed = parser.readFeed(lastBuildDate);
                if (feed != null && (oldfeed == null || oldfeed.getlastFetchDate() == null || !feed.getLastGuuid().equals(oldfeed.getLastGuuid()))) {
                    feed.setlastFetchDate(new Date());
                    M.datastore.save(feed);
                    Query<FeedAggregation> fas = M.datastore.find(FeedAggregation.class, "rssurls", feed.getRssUrl());
                    M.datastore.update(fas, M.datastore.createUpdateOperations(FeedAggregation.class).set("lastUpdate", feed.getlastFetchDate()));
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}

