package rssaggregator.models;

import java.util.Date;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Indexes;

@Entity
public class Feed {
    @Id
    private String rssurl;
    private String title;
    private String link;
    private String description;
    @Indexed
    private Date lastBuildDate;
    @Indexed
    private Date lastFetchDate;
    private String lastGuuid;

    public Feed() {
    }

    public Feed(String rssurl) {
        this.rssurl = rssurl;
    }

    public Feed(String rssurl, String title, String link, String description, Date lastBuildDate) {
        this.rssurl = rssurl;
        this.title = title;
        this.link = link;
        this.description = description;
        this.lastBuildDate = lastBuildDate;
    }

    public Date getLastBuildDate() {
        return this.lastBuildDate;
    }

    public void setLastBuildDate(Date date) {
        this.lastBuildDate = date;
    }

    public Date getlastFetchDate() {
        return this.lastFetchDate;
    }

    public void setlastFetchDate(Date date) {
        this.lastFetchDate = date;
    }

    public String getRssUrl() {
        return this.rssurl;
    }

    public String getLastGuuid() {
        return this.lastGuuid;
    }

    public void setLastGuuid(String guuid) {
        this.lastGuuid = guuid;
    }
}