package rssaggregator.models.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DateTimeFormatter {
    private static ArrayList<DateFormat> formatterList = new ArrayList<DateFormat>() {{
        add(new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.ENGLISH));
        add(new SimpleDateFormat("EEE, d MMM yyyy HH:mm z", Locale.ENGLISH));
    }};

    public static Date ParseString(String strDate) throws ParseException {
        for (DateFormat format: formatterList) {
            try {
				return format.parse(strDate);
			} catch (ParseException e) {
			}
        }
        throw new ParseException(strDate + " Can't be parsed", 0);
    }
}