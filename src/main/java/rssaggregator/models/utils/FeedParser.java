package rssaggregator.models.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;
import rssaggregator.models.FeedMessage;
import rssaggregator.services.M;
import rssaggregator.models.Feed;

public class FeedParser {
    static final String TITLE = "title";
    static final String DESCRIPTION = "description";
    static final String CHANNEL = "channel";
    static final String LANGUAGE = "language";
    static final String COPYRIGHT = "copyright";
    static final String LINK = "link";
    static final String AUTHOR = "author";
    static final String ITEM = "item";
    static final String PUB_DATE = "pubDate";
    static final String GUID = "guid";
    static final String COMMENTS = "comments";
    static final String ENCLOSURE = "enclosure";
    static final String SOURCE = "source";
    static final String LAST_BUILD_DATE = "lastBuildDate";

    final URL url;

    public FeedParser(String feedUrl) {
        try {
            this.url = new URL(feedUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public Feed readFeed(Date readLastTime) throws ParseException {
        Feed feed = null;
        try {
            boolean isFeedHeader = true;
            // Set header values intial to the empty string
            String description = "";
            String title = "";
            String link = "";
            String language = "";
            String copyright = "";
            String author = "";
            Date pubdate = null;
            String guid = "";
            String enclosure = "";
            String comments = "";
            String source = "";
            Date lastBuildDate = null;

            List<FeedMessage> fms = new ArrayList<>();

            // First create a new XMLInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            // Setup a new eventReader
            InputStream in = read();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // read the XML document
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName()
                            .getLocalPart();
                    switch (localPart) {
                    case ITEM:
                        if (isFeedHeader) {
                            isFeedHeader = false;
                            feed = new Feed(this.url.toString(), title, link, description, lastBuildDate);
                            title = "";
                            link = "";
                            description = "";
                            lastBuildDate = null;
                        }
                        event = eventReader.nextEvent();
                        break;
                    case TITLE:
                        title = getCharacterData(event, eventReader);
                        break;
                    case DESCRIPTION:
                        description = getCharacterData(event, eventReader);
                        break;
                    case LINK:
                        link = getCharacterData(event, eventReader);
                        break;
                    case GUID:
                        guid = getCharacterData(event, eventReader);
                        break;
                    case LANGUAGE:
                        language = getCharacterData(event, eventReader);
                        break;
                    case AUTHOR:
                        author = getCharacterData(event, eventReader);
                        break;
                    case PUB_DATE:
                        pubdate = DateTimeFormatter.ParseString(getCharacterData(event, eventReader));
                        break;
                    case COPYRIGHT:
                        copyright = getCharacterData(event, eventReader);
                        break;
                    case COMMENTS:
                        comments = getCharacterData(event, eventReader);
                        break;
                    case ENCLOSURE:
                        enclosure = getCharacterData(event, eventReader);
                        break;
                    case LAST_BUILD_DATE:
                        lastBuildDate = DateTimeFormatter.ParseString(getCharacterData(event, eventReader));
                        if (readLastTime != null && !lastBuildDate.after(readLastTime)) {
                            return null;
                        }
                        break;
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
                        FeedMessage message = new FeedMessage(this.url.toString(), title, link, description, author, comments, enclosure, guid, source, pubdate);
                        if (feed.getLastGuuid() == null) {
                            feed.setLastGuuid(guid);
                        }
                        fms.add(message);
                        event = eventReader.nextEvent();
                        continue;
                    }
                }
            }
            M.datastore.save(fms);
            return feed;
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
    }

    private String getCharacterData(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }

    private InputStream read() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}