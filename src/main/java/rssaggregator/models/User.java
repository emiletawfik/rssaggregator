package rssaggregator.models;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import rssaggregator.models.utils.Hash;


@Entity
public class User {
    @Id
    private String uuid;
    private String username;
    private String password;

    public User() {
        this.uuid = UUID.randomUUID().toString();
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) throws NoSuchAlgorithmException {
        this.password = Hash.hash(password);
    }
}