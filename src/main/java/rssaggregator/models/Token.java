package rssaggregator.models;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.UUID;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexes;

@Entity
@Indexes(@Index(fields = { @Field("uuid"), @Field("user_uuid") }, options = @IndexOptions(unique = true)))
public class Token {
    @Id
    private String uuid;
    private String user_uuid;
    private Date expiration_date;

    public Token() {
        this.uuid = UUID.randomUUID().toString();
    }

    public Token(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getUserUuid() {
        return this.user_uuid;
    }

    public void setUserUuid(String user_uuid) throws NoSuchAlgorithmException {
        this.user_uuid = user_uuid;
    }

    public Date getExpirationDate() {
        return this.expiration_date;
    }

    public void setExpirationDate(Date date) {
        this.expiration_date = date;
    }

}