package rssaggregator.models;

import java.util.Date;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Indexes;

@Entity
@Indexes(@Index(fields = { @Field("guid"), @Field("rssurlsource") }, options = @IndexOptions(unique = true)))
public class FeedMessage {
    @Id
    private String uuid;
    private String title;
    private String link;
    private String description;
    private String author;
    private String comments;
    private String enclosure;
    private String guid;
    private String source;
    @Indexed
    private Date pubDate;
    @Indexed
    private String rssurlsource;

    public FeedMessage() {
    }

    public FeedMessage(String rssurlsource, String title, String link, String description, String author,
            String comments, String enclosure, String guid, String source, Date pubDate) {
        this.uuid = rssurlsource + guid;
        this.rssurlsource = rssurlsource;
        this.title = title;
        this.link = link;
        this.description = description;
        this.author = author;
        this.comments = comments;
        this.enclosure = enclosure;
        this.guid = guid;
        this.source = source;
        this.pubDate = pubDate;
    }

    public Date getPubDate() {
        return this.pubDate;
    }
}