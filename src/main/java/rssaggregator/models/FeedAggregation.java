package rssaggregator.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

@Entity
public class FeedAggregation {
    @Id
    private String uuid;
    @Indexed
    private String user_uuid;
    private String name;
    @Indexed
    private Date lastUpdate;

    @Embedded
    private List<String> rssurls = new ArrayList<String>();

    public FeedAggregation() {
        this.uuid = UUID.randomUUID().toString();
    }

    public FeedAggregation(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getUserUuid() {
        return this.user_uuid;
    }

    public void setUserUuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public List<String> getRssUrls() {
        return this.rssurls;
    }

    public String getName() {
        return this.name;
    }

    public Date getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(Date date) {
        this.lastUpdate = date;
    }
}