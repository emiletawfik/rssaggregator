# Launch Api
```bash
docker-compose build && docker-compose up
```
Output
```
rssaggregator_1  | >> Listening on 0.0.0.0:4567
```
# Api Calls

### Create a new user
Request
```HTTP
POST /users
Content-Type: application/json
{
	"username": "jean",
	"password": "michel"
}
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
    "data": {
        "uuid": "53d5971a-f0fd-4010-afcc-9fa386df8c2b",
        "username": "jean",
        "password": "2c7f9fd20fbeb41ce8894ec4653d66fa7f3b6e1a"
    }
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "status": "ERROR",
    "message": "Username already used"
}
```

### Create a new Token
Request
```HTTP
POST /newToken
Content-Type: application/json
{
	"username": "jean",
	"password": "michel"
}
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
    "data": {
        "uuid": "89fb5ecf-068c-4f14-92a2-d9f9f3b60d37",
        "user_uuid": "53d5971a-f0fd-4010-afcc-9fa386df8c2b",
        "expiration_date": "Feb 27, 2018 10:19:56 PM"
    }
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "status": "ERROR",
    "message": "Can't find username or password"
}
```

### Wrong Token Responses
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: {application/json}
{
    "status": "ERROR",
    "message": "You must specify authorization header to use this call"
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: {application/json}
{
    "status": "ERROR",
    "message": "This token is expired please use a new one"
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: {application/json}
{
    "status": "ERROR",
    "message": "Your are authentified with a wrong token"
}
```

### Change password (ignore username)
Request
```HTTP
POST /users/changepassword
Content-Type: application/json
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
{
	"password": "michel"
}
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
    "data": {
        "uuid": "53d5971a-f0fd-4010-afcc-9fa386df8c2b",
        "username": "jean",
        "password": "2c7f9fd20fbeb41ce8894ec4653d66fa7f3b6e1a"
    }
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: {application/json}
{
    "status": "ERROR",
    "message": "The user associated to the token didn't exist"
}
```

### Delete User
Request
```HTTP
DELETE /users/delete
Content-Type: application/json
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: {application/json}
{
    "status": "ERROR",
    "message": "The user associated to the token didn't exist"
}
```

### Feeds

#### Get Feeds
Request
```HTTP
GET /feedaggregators
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
    "data": [
        {
            "uuid": "e343f684-3ba5-4759-be68-d35a17168176",
            "user_uuid": "655501a2-d9ff-4f6f-abde-fff32fc4213e",
            "name": "swag aggregator",
            "rssurls": [
                "http://www.feedforall.com/sample.xml",
                "http://www.feedforall.com/sample-feed.xml"
            ]
        },
        {
            "uuid": "3e4cf0de-4255-458c-b91c-f389511ee985",
            "user_uuid": "655501a2-d9ff-4f6f-abde-fff32fc4213e",
            "name": "swag aggregator",
            "lastUpdate": "Feb 2, 2017 2:00:18 PM",
            "rssurls": [
                "http://www.feedforall.com/sample.xml",
                "http://www.feedforall.com/sample-feed.xml",
                "http://www.feedforall.com/blog-feed.xml",
                "http://www.rss-specifications.com/blog-feed.xml"
            ]
        }
    ]
}
```

#### Add or Update Feeds
Request
```HTTP
POST /feedaggregators
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
Content-type: application/json
{
    "name": "swag aggregator",
	"rssurls": [
		"http://www.feedforall.com/sample.xml",
		"http://www.feedforall.com/sample-feed.xml"
	]
}
```
```HTTP
POST /feedaggregators
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
Content-type: application/json
{
	"uuid": "e343f684-3ba5-4759-be68-d35a17168176",
    "name": "swag aggregator",
	"rssurls": [
		"http://www.feedforall.com/sample.xml",
		"http://www.feedforall.com/sample-feed.xml"
	]
}
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
    "data": [
        {
            "uuid": "e343f684-3ba5-4759-be68-d35a17168176",
            "user_uuid": "655501a2-d9ff-4f6f-abde-fff32fc4213e",
            "rssurls": [
                "http://www.feedforall.com/sample.xml",
                "http://www.feedforall.com/sample-feed.xml"
            ]
        },
        {
            "uuid": "3e4cf0de-4255-458c-b91c-f389511ee985",
            "user_uuid": "655501a2-d9ff-4f6f-abde-fff32fc4213e",
            "rssurls": [
                "http://www.feedforall.com/sample.xml",
                "http://www.feedforall.com/sample-feed.xml",
                "http://www.feedforall.com/blog-feed.xml",
                "http://www.rss-specifications.com/blog-feed.xml"
            ]
        }
    ]
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "status": "ERROR",
    "message": "You can't update this aggregation because your are not his owner"
}
```

#### Delete User Feeds
Request
```HTTP
DELETE /feedaggregators/{feedaggregators-uuid}
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS"
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "status": "ERROR",
    "message": "You can't delete this aggregation because your are not his owner"
}
```

#### Get the message in the feed all the feed contained by an aggregator
Request
```HTTP
GET /feedaggregators/{feedaggregators-uuid}
Authorization: 89fb5ecf-068c-4f14-92a2-d9f9f3b60d37
```
Responses
```HTTP
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "SUCCESS",
    "data": [
        {
            "uuid": "http://www.feedforall.com/blog-feed.xml953",
            "title": "How Do I Promote My Podcast?",
            "link": "http://www.feedforall.com/blog.htm#953",
            "description": "There are a variety of things you can do to promote your podcast and increase its popularity... ",
            "author": "",
            "comments": "",
            "enclosure": "",
            "guid": "953",
            "source": "",
            "pubDate": "Feb 2, 2019 2:00:18 PM",
            "rssurlsource": "http://www.feedforall.com/blog-feed.xml"
        },
             {
            "uuid": "http://www.feedforall.com/sample.xml",
            "title": "RSS Solutions for Law Enforcement",
            "link": "http://www.feedforall.com/law-enforcement.htm",
            "description": "<",
            "author": "",
            "comments": "http://www.feedforall.com/forum",
            "enclosure": "",
            "guid": "",
            "source": "",
            "pubDate": "Oct 19, 2004 3:08:56 PM",
            "rssurlsource": "http://www.feedforall.com/sample.xml"
        }
    ]
}
```
```HTTP
HTTP/1.1 400 Bad Request
Content-Type: application/json
{
    "status": "ERROR",
    "message": "You can't read this aggregation because your are not his owner"
}
```